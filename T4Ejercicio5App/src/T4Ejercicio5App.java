/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T4Ejercicio5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Inicializo las variables
		int A = 10 , B = 20, C = 30, D = 40;
		
		// Muestro las variables 
		System.out.println(A + ", " + B + ", " + C + ", " + D);
		System.out.println("");
		
		// Cambio los valores
		B = C;
		
		// Muestro el nuevo valor
		System.out.println(B);
		
		// Cambio los valores
		C = A;
		
		// Muestro el nuevo valor
		System.out.println(C);
		
		// Cambio los valores
		A = D;
		
		// Muestro el nuevo valor
		System.out.println(A);
		
		// Cambio los valores
		D = B;
		
		// Muestro el nuevo valor
		System.out.println(D);
		
		System.out.println("");
		
		// Mostramos los valores
		System.out.println(A + ", " + B + ", " + C + ", " + D);
	}

}
